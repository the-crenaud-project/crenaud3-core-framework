package scenecontroller;

import java.io.InputStream;

/**
 * Internal package class for unit tests
 *
 * @author klmp200
 */
class MenuTest extends Scene {

    /**
     * Feed the scene with it's config file
     *
     */
    public MenuTest() {
        System.out.println(configFile);
    }

    /**
     * Called from the SceneController
     * Launch the scene and show the window
     * Attach the controller to the scene to access next level
     *
     * @param controller SceneController
     */
    @Override
    public void play(SceneController controller, InputStream settings) {
        super.play(controller, settings);
        System.out.println("MenuTest started");
    }

    /**
     * Called by the SceneController
     * Should close the window and unload everything
     */
    @Override
    public void end() {
        System.out.println("MenuTest closed");
    }

}
