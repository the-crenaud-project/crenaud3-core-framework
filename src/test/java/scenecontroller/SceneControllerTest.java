package scenecontroller;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test the SceneController class
 *
 * @author klmp200
 */
class SceneControllerTest {

    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    /**
     * Change outputs to easily test them
     */
    @BeforeAll
    static void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(outContent));
    }

    /**
     * Reset outputs content after each test methods
     */
    @AfterEach
    void resetUpStream(){
        outContent.reset();
        errContent.reset();
    }

    /**
     * Set back outputs to normal
     */
    @AfterAll
    static void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    /**
     * Test toString function of a SceneController
     * Check correct loading from .godwin file
     */
    @Test
    void toStringTest() {
        SceneController controller = new SceneController("/testController.godwin");
        String test = "SceneController{currentScene=0, " + "sceneNumber=6, " +
                "sceneList=[ImportedScene{type='scenecontroller.MenuTest', settingFilePath=/main/menu/path}, " +
                "ImportedScene{type='dialogue', settingFilePath=/first/dialogue/path}, " +
                "ImportedScene{type='dialogue', settingFilePath=/second/dialogue/path}, " +
                "ImportedScene{type='dialogue', settingFilePath=/third/dialogue/path}, " +
                "ImportedScene{type='dialogue', settingFilePath=/fourth/dialogue/path}, " +
                "ImportedScene{type='dialogue', settingFilePath=/fifth/dialogue/path}]}";
        assertEquals(test,controller.toString());
    }

    /**
     * Use case of a SceneController
     */
    @Test
    void sceneLoadTest() {
        SceneController controller = new SceneController("/testController.godwin");
        Scene testScene;

        // Test if returned first scene is a menu and not instantiated twice when called twice
        testScene = controller.getCurrentScene();
        assertTrue(testScene instanceof MenuTest);
        assertTrue(controller.getCurrentScene() instanceof MenuTest);
        assertTrue(testScene == controller.getCurrentScene());

        // Current scene not finished, should return null
        assertTrue(controller.getNextScene() == null);

        // Current scene has a not found config file
        assertEquals("null\n", outContent.toString());
        outContent.reset();

        // Test correct play and end of menu
        testScene.play(controller, getClass().getResourceAsStream("bite"));
        assertEquals("MenuTest started\n", outContent.toString());
        outContent.reset();
        testScene.end();
        assertEquals("MenuTest closed\n", outContent.toString());

        // Test that second test not instantiated since not found
        testScene = controller.getNextScene();
        assertFalse(testScene instanceof MenuTest);
        assertTrue(testScene == null);
    }


}