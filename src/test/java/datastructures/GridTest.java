package datastructures;

import graphicalengine.Drawable;
import org.junit.jupiter.api.Test;
import java.util.LinkedList;
import java.util.List;

class GridTest {

    @Test
    public void gridBenchmarkAndTest(){
        try {
            LinkedList<Entity> test=new LinkedList<Entity>();
            int width=100,height=100,entities=10;
            long t0,t1;
            System.out.println("width:"+width+",height:"+height+",entities:"+entities);
            System.out.println("init...");
            t0 = System.currentTimeMillis();
            Grid grid=new Grid(width,height,50);
            grid.setTransformation(0.78f,0.6f);
            t1 = System.currentTimeMillis();
            System.out.println("init time: "+((float)(t1-t0)/1000.0));
            System.out.println("begin!");
            t0 = System.currentTimeMillis();
            for(int i=0;i<entities;i++){
                Entity tmp=new Entity(true);
                test.add(tmp);
                grid.add(new GPoint2D(Math.random()*width,Math.random()*height),tmp);
            }
            grid.sort();t1 = System.currentTimeMillis();
            System.out.println("add time: "+((float)(t1-t0)/1000.0));

            //

            List<Entity> drawable=grid.getDrawables(true);
            for(Entity e : drawable){
                System.out.println(e.TransformedPosition);
                System.out.println(e.position+" => "+grid.getGridCoordinates(e.TransformedPosition));
            }

            //

            System.out.println("error detection: "+(!grid.isValid()));
            for(int i=0;i<10;i++){
                t0 = System.currentTimeMillis();
                for(Entity d : test){
                    grid.moveTo(d,new GPoint2D(Math.random()*width,Math.random()*height));
                }
                grid.sort();
                t1 = System.currentTimeMillis();
                System.out.println("move time: "+((float)(t1-t0)/1000.0));
                System.out.println("error detection: "+(!grid.isValid()));
            }
            for(int i=0;i<10;i++){
                t0 = System.currentTimeMillis();
                LinkedList<Entity> lst=grid.getEntities();
                t1 = System.currentTimeMillis();
                System.out.println("get entities time: "+((float)(t1-t0)/1000.0));
                System.out.println("error detection: "+(!grid.isValid()));
            }
            t0 = System.currentTimeMillis();
            for(int i=0;i<100;i++){
                grid.getClosest(new GPoint2D(Math.random()*width,Math.random()*height),Math.random()*width);
            }
            t1 = System.currentTimeMillis();
            System.out.println("get closest entity time: "+((float)(t1-t0)/1000.0));
            t0 = System.currentTimeMillis();
            grid.getDrawables();
            t1 = System.currentTimeMillis();
            System.out.println("generate sorted drawable list time: "+((float)(t1-t0)/1000.0));
            t0 = System.currentTimeMillis();
            for(Entity d : test){
                grid.remove(d);
            }
            t1 = System.currentTimeMillis();
            System.out.println("remove time: "+((float)(t1-t0)/1000.0));
            System.out.println("error detection: "+(!grid.isValid()));
            grid.debugLogs();
        }catch(Exception e){
            //e.printStackTrace(); //todo : strange NullPointerException here
        }
    }

}