package physics;


import datastructures.GPoint2D;
import org.junit.jupiter.api.Test;
import physics.exceptions.NullSpeedException;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by aethor on 03/12/17.
 */
class WorldTest {

    @Test
    public void worldTest(){

        World world = World.getInstance(10, 10);

        PhysicalEntity physicalEntity = new PhysicalEntity(new GPoint2D(1, 1), 1, 1, 1, true, true, true);

        try {
            physicalEntity.move(Direction.LEFT);
        } catch (NullSpeedException e) {
            e.printStackTrace();
        }

        world.add(physicalEntity);
        world.update(1);

        //assertTrue(physicalEntity.getPhysicalPosition().getX() == 0);
    }
}
