package graphicalengine;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * The interface allowing something to be displayed by the camera
 *
 * @author Naej Doree
 */
public interface Drawable {

    /**
     * @return the position of the drawable
     */
    Point2D getPosition();

    /**
     * @return the image that will be displayed
     */
    Image getSprite();

    /**
     * Allows the camera to know if the Sprite is reversed on the X axis
     * @return false for the default image
     */
    boolean isFlippedX();


    /**
     * Allows the camera to know if the Sprite is reversed on the Y axis
     * @return false for the default image
     */
    boolean isFlippedY();


}
