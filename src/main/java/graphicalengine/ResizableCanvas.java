package graphicalengine;

import javafx.scene.canvas.Canvas;

/**
 * A resizable Canvas for a cool interface
 * @author klmp200
 */
public class ResizableCanvas extends Canvas {
    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }


    @Override
    public double prefHeight(double width) {
        return getHeight();
    }
}
