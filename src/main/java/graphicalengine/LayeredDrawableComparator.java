package graphicalengine;

import java.util.Comparator;

public class LayeredDrawableComparator implements Comparator<LayeredDrawable> {
    @Override
    public int compare(LayeredDrawable o1, LayeredDrawable o2) {
        return Float.compare(o1.getLayer(),o2.getLayer());
    }
}
