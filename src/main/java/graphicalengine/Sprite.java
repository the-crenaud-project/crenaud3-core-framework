package graphicalengine;


import javafx.geometry.Point2D;
import javafx.scene.image.Image;


/**
 * A basic sprite class
 *
 * @author Naej Doree
 */
public class Sprite implements Drawable{
    private Image sprite;
    private Point2D position;
    private boolean flippedX=false;
    private boolean flippedY=false;

    /**
     * Constructor of a sprite
     * @param _sprite the image that will be displayed
     * @param _position the position of the sprite
     */
    public Sprite(Image _sprite,Point2D _position){
        this.sprite = _sprite;
        this.position =_position;
    }

    /**
     * Constructor of a sprite
     * @param url the url of the image that will be displayed
     * @param _position the position of the sprite
     */
    public Sprite(String url,Point2D _position){
        sprite = new Image(url);
        position = _position;
    }

    public Point2D getPosition() {
        return position;
    }

    public Image getSprite() {
        return sprite;
    }

    public void setFlip(boolean X,boolean Y){
        flippedX = X;
        flippedY = Y;
    }

    public void setPosition(Point2D pos){
        position = pos;
    }

    public boolean isFlippedX(){
        return flippedX;
    }
    public boolean isFlippedY(){
        return flippedY;
    }


    public static Point2D offsetCenterOfImage(Image spr){
        return new Point2D(spr.getHeight()/2,spr.getWidth()/2);
    }

}
