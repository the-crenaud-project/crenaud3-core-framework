package graphicalengine;

import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * The object used to display content
 *
 * @author Naej Doree
 */
public class Camera {
    private ArrayList<LayeredDrawable> overlay = new ArrayList<LayeredDrawable>();
    private ArrayList<LayeredDrawable> background = new ArrayList<LayeredDrawable>();
    private float zoom = 1;
    private Point2D position = new Point2D(0,0);
    private Canvas cv;
    private GraphicsContext gc;

    /**
     * Constructor of the camera
     * @param cv canvas where the content of the camera will be displayed
     */
    public Camera(Canvas cv){
        this.cv = cv;
        gc = cv.getGraphicsContext2D();
        overlay = new ArrayList<LayeredDrawable>();
        background = new ArrayList<LayeredDrawable>();
        position = new Point2D(0,0);
        zoom = 1;
    }
    /**
     * Allows you to move the camera
     * @param movement vector representing the movement of the camera
     */
    public void move(Point2D movement){
        position = position.add(movement);
    }

    /**
     * Allows you to know where the camera is
     * @return the camera position
     */
    public Point2D getPosition(){
        return position;
    }

    /**
     * Fuction used to refresh the canvas and display the content
     * @param grid the isometric grid of objects
     */
    public void draw(LinkedList<Drawable> grid){
        gc.clearRect(0,0,cv.getWidth()*1/zoom,cv.getHeight()*1/zoom);

        for(Drawable toDraw:background) {
            drawSomething(toDraw);
        }
        for(Drawable toDraw:grid) {
            drawSomething(toDraw);
        }
        for(Drawable toDraw:overlay) {
            drawSomething(toDraw);
        }
    }

    /**
     * Allows you to set the zoom of the camera
     * @param zoom
     */
    public void setZoom(float zoom){

        cv.getGraphicsContext2D().scale(1/this.zoom,1/this.zoom);
        gc.scale(zoom,zoom);
        this.zoom = zoom;
    }

    /**
     * Allows you an easier zoom in and out
     * @param zoom
     */
    public void multiplyZoom(float zoom){
        cv.getGraphicsContext2D().scale(zoom,zoom);
        this.zoom *= zoom;

    }

    /**
     * returns the level of zoom
     * @return
     */
    public float getZoom(){
        return zoom;

    }


    /**
     * Transform a point from the canvas ref to the CameraView ref
     * @param pos the point
     * @return the transformed point
     */
    public Point2D fromCameraViewToCanvas(Point2D pos){
        return new Point2D(pos.getX()*zoom-this.position.getX(),pos.getY()*zoom-this.position.getY());
    }

    /**
     * Transform a point from the CameraView ref to the canvas ref
     * @param pos the point
     * @return the transformed point
     */
    public Point2D fromCanvasToCameraView(Point2D pos){
        return new Point2D(pos.getX()/zoom+this.position.getX(),pos.getY()/zoom+this.position.getY());
    }


    /**
     * A function that allows to test the correct functionality of the camera
     * @return true if the test worked perfectly
     */
    public boolean test(){
        Point2D pt1 = new Point2D(15,15);
        Point2D pt2 = fromCameraViewToCanvas(fromCanvasToCameraView(pt1));
        return pt1.equals(pt2);
    }

    /**
     * Allows you to draw something without wondering about anything more
     * @param toDraw thing that will be drawn
     */
    public void drawSomething(Drawable toDraw) {
        if (!toDraw.isFlippedX() && !toDraw.isFlippedY()) {
            gc.drawImage(toDraw.getSprite(), toDraw.getPosition().subtract(position).getX(), toDraw.getPosition().subtract(position).getY());
        }else if (toDraw.isFlippedY() && !toDraw.isFlippedX()) {
            gc.drawImage(toDraw.getSprite(),
                    0, 0,
                    toDraw.getSprite().getWidth(), toDraw.getSprite().getHeight(),
                    toDraw.getPosition().subtract(position).getX()+toDraw.getSprite().getWidth(), toDraw.getPosition().subtract(position).getY(),
                    -toDraw.getSprite().getWidth(),toDraw.getSprite().getHeight());

        }else if (toDraw.isFlippedX() && !toDraw.isFlippedY()){
            gc.drawImage(toDraw.getSprite(),
                    0, 0,
                    toDraw.getSprite().getWidth(), toDraw.getSprite().getHeight(),
                    toDraw.getPosition().subtract(position).getX(), toDraw.getPosition().subtract(position).getY()+toDraw.getSprite().getHeight(),
                    toDraw.getSprite().getWidth(),-toDraw.getSprite().getHeight());
        }else if (toDraw.isFlippedX() && toDraw.isFlippedY()){
            gc.drawImage(toDraw.getSprite(),
                    0, 0,
                    toDraw.getSprite().getWidth(), toDraw.getSprite().getHeight(),
                    toDraw.getPosition().subtract(position).getX()+toDraw.getSprite().getWidth(), toDraw.getPosition().subtract(position).getY()+toDraw.getSprite().getHeight(),
                    -toDraw.getSprite().getWidth(),-toDraw.getSprite().getHeight());
        }else{
            System.out.println("Wtf do you want me to do, your image have strange flipping values");
        }
    }

    public void addOverlay(Drawable d, float layer){
        overlay.add(new LayeredDrawable(d,layer));
        overlay.sort(new LayeredDrawableComparator());
    }
    public void addBackground(Drawable d, float layer){
        background.add(new LayeredDrawable(d,layer));
        background.sort(new LayeredDrawableComparator());
    }
    public void clearOverlay(){
        overlay.clear();
    }
    public void clearBackground(){
        background.clear();
    }

}
