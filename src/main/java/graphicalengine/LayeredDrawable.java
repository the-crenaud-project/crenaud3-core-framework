package graphicalengine;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;

public class LayeredDrawable implements Drawable {
    /**
     * The class allowing something to be used as overlay or background by the camera
     *
     * @author Naej Doree
     */
    protected Drawable toDraw;
    protected float layer;

    public LayeredDrawable(Drawable d,float layer){
        toDraw = d;
        this.layer = layer;
    }

    public float getLayer() {
        return layer;
    }

    @Override
    public Point2D getPosition() {
        return toDraw.getPosition();
    }

    @Override
    public Image getSprite() {
        return toDraw.getSprite();
    }

    @Override
    public boolean isFlippedX() {
        return toDraw.isFlippedX();
    }

    @Override
    public boolean isFlippedY() {
        return toDraw.isFlippedY();
    }
}
