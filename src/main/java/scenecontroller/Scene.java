package scenecontroller;

import java.io.InputStream;

/**
 * Interface of a loaded Scene
 *
 * @author klmp200
 */
public abstract class Scene {
    protected SceneController controller;
    protected InputStream configFile;
    protected String fxmlPath;
    protected String cssPath;

    /**
     * Called from the SceneController
     * Launch the scene and show the window
     * Attach the controller to the scene to access next level
     * @param controller SceneController
     */
    public void play(SceneController controller, InputStream configFile){
        this.controller = controller;
    }

    /**
     * Get FXML path
     * @return String
     */
    public String getFxmlPath() {
        return fxmlPath;
    }

    /**
     * Get CSS path
     * @return String
     */
    public String getCssPath() {
        return cssPath;
    }

    /**
     * Called by the SceneController
     * Should close the window and unload everything
     */
    public abstract void end();
}
