package scenecontroller;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.*;

/**
 * Abstract object for implementing a game controller
 * This game controller launch scenes following a pattern given in a .godwin file
 * It also save scoring, save and reload a game
 *
 * @author klmp200
 */
public class SceneController {
    private int currentScene;
    private List<ImportedScene> sceneList;
    private Scene currentSceneInstance;
    private FXMLLoader currentLoader;
    private javafx.scene.Scene jfxScene;
    private Stage stage;
    private boolean isSceneFinished;


    /**
     * Constructor of the Scene controller
     * @param controllerFilePath .godwin file
     */
    public SceneController(String controllerFilePath) {
        Scanner controllerFile = new Scanner(getClass().getResourceAsStream(controllerFilePath));
        currentScene = 0;
        sceneList = new ArrayList<ImportedScene>();
        currentSceneInstance = null;

        while (controllerFile.hasNextLine()) sceneList.add(new ImportedScene(controllerFile.nextLine().split(":")));
    }

    /**
     * Load the current scene
     * @throws IOException if error importing files
     */
    private void loadScene() throws IOException {
        if (getCurrentScene() != null){
            if (currentSceneInstance.getCssPath() != null)
                loadFXML(currentSceneInstance.getFxmlPath(), currentSceneInstance.getCssPath());
            else loadFXML(currentSceneInstance.getFxmlPath());

            currentSceneInstance.play(this, sceneList.get(currentScene).settingFile);
        }
    }

    /**
     * Start the game with the first scene
     * @param primaryStage javafx Stage
     * @throws IOException if no stage at all or error with loading controller
     */
    public void play(Stage primaryStage) throws IOException {
        isSceneFinished = true;
        stage = primaryStage;
        loadScene();
        stage.show();
    }

    /**
     * Play the next Scene
     * @throws IOException if no next stage available
     */
    public void playNext() throws IOException {
        currentSceneInstance.end();
        currentSceneInstance = null;
        currentSceneInstance = getNextScene();
        isSceneFinished = false;
        loadScene();
        stage.show();
    }

    /**
     * Get current instantiated scene
     * Instantiate new scene if no currently instantiated
     * Return null if no finder have been set
     * @return current scene
     */
    Scene getCurrentScene(){
        ImportedScene tmp = null;
        if (currentScene < sceneList.size()) {
            if (currentSceneInstance == null) {
                try {
                    // Use black magic to import class from string name
                    tmp = sceneList.get(currentScene);
                    currentSceneInstance = (Scene) Class.forName(tmp.type).newInstance();
                } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                return currentSceneInstance;
            }
            return currentSceneInstance;
        }
        return null;
    }

    /**
     * Get next scene if available
     * @return next scene or null
     */
    Scene getNextScene(){
        if (hasNextScene() && isSceneFinished){
            currentScene++;
            return getCurrentScene();
        }
        return null;
    }

    /**
     * Inform if a scene is available after the current scene
     * @return boolean
     */
    private boolean hasNextScene(){
        return currentScene + 1 < sceneList.size();
    }


    /**
     * Called by the scene when it ends
     * @// TODO: 21/11/2017 Add SceneData communication 
     */
    public void sceneEnded(){
        isSceneFinished = true;
        if (hasNextScene()) {
            try {
                playNext();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else Platform.exit();
    }

    /**
     * Convent a controller to a string
     * Useful for tests
     * @return String
     */
    @Override
    public String toString() {
        return "SceneController{" +
                "currentScene=" + currentScene +
                ", sceneNumber=" + sceneList.size() +
                ", sceneList=" + sceneList +
                '}';
    }

    /**
     * This class describe a non instantiated scene
     * Those are loaded from the .godwin file
     * @author klmp200
     */
    private static class ImportedScene {
        private String type;
        private String settingFilePath;
        private InputStream settingFile;

        /**
         * Function used to convert the object to String
         * @return String
         */
        @Override
        public String toString() {
            return "ImportedScene{" +
                    "type='" + type + '\'' +
                    ", settingFilePath=" + settingFilePath +
                    '}';
        }

        /**
         * Constructor for the ImportedScene object
         * @param argv [scene type, path of config json file]
         */
        ImportedScene(String[] argv) {
            this.type = argv[0];
            this.settingFilePath = argv[1];
            this.settingFile = getClass().getResourceAsStream(settingFilePath);
        }
    }

    /**
     * Close the window and kill everything
     */
    private void closeWindow(){
        if (currentLoader != null) {
            currentSceneInstance.end();
        }
        currentLoader = null;
        jfxScene = null;
    }

    /**
     * Create a new Scene and give it a fxml and a css
     * @param fxml as String
     * @param css as String
     * @throws IOException for errors while handling files
     */
    private void loadFXML(String fxml, String css) throws IOException {
        loadFXML(fxml);
        jfxScene.getStylesheets().add(getClass().getClassLoader().getResource(css).toExternalForm());
    }

    /**
     * Create a new Scene and give it a fxml and a css
     * @param fxml as String
     * @throws IOException for errors while handling files
     */
    private void loadFXML(String fxml) throws IOException {
        URL location = getClass().getClassLoader().getResource(fxml);
        closeWindow();
        currentLoader = new FXMLLoader();
        currentLoader.setLocation(location);
        currentLoader.setBuilderFactory(new JavaFXBuilderFactory());
        assert location != null;
        Parent root = currentLoader.load(location.openStream());
        jfxScene = new javafx.scene.Scene(root);
        stage.setScene(jfxScene);

        // get the new instance generated by javafx. Trust me, I'm an engineer !
        currentSceneInstance = currentLoader.getController();
    }
}
