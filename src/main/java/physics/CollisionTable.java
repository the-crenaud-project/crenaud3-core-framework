
package physics;

/**
 * contains a collision mask and collisions layers. Is used by a hitbox to determine if two objects are colliding.
 * A collide with B if A mask and B layers have a bit in common.
 * @author Aethor
 */
public class CollisionTable {

    private byte layers;
    private byte mask;

    /**
     * main constructor for a TestTable
     * @param layers
     * @param mask
     */
    public CollisionTable(byte layers, byte mask){
        this.layers = layers;
        this.mask = mask;
    }


    /**
     * this TestTable collide with the other one if result of the & bitwise operation between this TestTable's mask and the
     * other TestTable's layers contains at least a 1
     * @param otherTable the other TestTable
     * @return
     */
    public boolean isColliding(CollisionTable otherTable){
        return ((mask & otherTable.layers) != 0);
    }

    /**
     * Returns the string representation of a collision table
     * @return
     */
    public String toString(){
        return "TestTable :\n\tmask : " + mask + "\n\tlayers : " + layers + "\n";
    }

}
