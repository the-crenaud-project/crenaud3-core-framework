package physics;

import datastructures.Entity;
import datastructures.GPoint2D;
import datastructures.Grid;
import datastructures.exception.CantFindChunkException;
import datastructures.exception.OutOfBoundsException;
import physics.exceptions.NullSpeedException;

/**
 * Crenaud3 Dynamic Object class.
 * @author Aethor
 */
public class PhysicalEntity extends Entity{

    private Hitbox hitbox;
    private double speed;
    private GPoint2D speedVector;
    private Direction direction;
    private boolean movable;
    private boolean collidable;
    private boolean collidedLastFrame;
    private GPoint2D lastPhysicalPosition;
    private Grid grid;

    /**
     * Main constructor for a PhysicalEntity. At the beginning, a PhysicalEntity doesn't have any moving direction. Assign one
     * using the move function
     * @param position the position of the movable object ( top left corner wise )
     * @param height the height of the object ( used to construct it's hitbox )
     * @param speed the speed norm of the object. If it's negative, the speed is automatically corrected to 0
     * @param drawable
     */
    public PhysicalEntity(GPoint2D position, int height, int width, double speed, boolean drawable, boolean movable, boolean collidable){

        super(drawable, position);

        this.direction = Direction.NONE;
        this.speedVector = new GPoint2D(0, 0);
        this.movable = movable;
        this.collidable = collidable;
        this.speed = (speed >= 0 && isMovable())?speed:0;
        this.hitbox = (isCollidable())?new Hitbox(height, width, new CollisionTable((byte) 0b11111111, (byte) 0b11111111)):null;
        this.collidedLastFrame = false;
        lastPhysicalPosition = new GPoint2D();

    }


    /**
     * used to move the object. It will modifiy the object's direction, which will be used when updating position next frame.
     * It is worth noting that the object will not move if an object is in it's direct path due to colliding. Also, when
     * attempting to move an object with no speed, a exception will be throwed
     * @param direction the direction in which the object shall start to move.
     */
    public void move(Direction direction) throws NullSpeedException {

        if(isMovable()){
            this.direction = direction;
            if(this.speed == 0){
                throw new NullSpeedException("Attempting to move an object with a null speed");
            }
            switch (direction){
                case LEFT: speedVector.setLocation(-speed, 0);
                    break;
                case UP: speedVector.setLocation(0, -speed);
                    break;
                case RIGHT: speedVector.setLocation(speed, 0);
                    break;
                case DOWN: speedVector.setLocation(0, speed);
                    break;
            }
        }

    }


    /**
     * set the object speed to 0, effectively stopping it
     */
    public void stop(){
        speedVector.setLocation(0, 0);
    }


    /**
     * Launched every frame. Unless overriden, will launch the the physical update function of the object ( which makes it
     * move ) and the custom update function of the object ( empty by default, but you can override it )
     * @param lambda the duration of a frame
     */
    protected void update(double lambda){
        customUpdate(lambda);
        if(isMovable()){
            physicalUpdate(lambda);
        }
    }

    /**
     * update the position of the object every frame. Cannot be overriden, but you can avoid launching it by suppressing it's
     * call if you override the "update" function ( not recommended )
     * @param lambda the duration of the frame
     */
    private void physicalUpdate(double lambda){
            GPoint2D newLocation = new GPoint2D(position.getX() + speedVector.getX() * lambda,
                    position.getY() + speedVector.getY() * lambda);
            moveTo(newLocation);
    }


    /**
     * Custom update function, lauched every frame. Empty by default, but you can override it.
     * @param lambda the duration of the frame
     */
    protected void customUpdate(double lambda){

    }


    /**
     * Custom collision function. Does nothing here, but can be overrided to set up custom collision behaviour
     * @param collidingObject The object this one collided with
     * @param lambda the duration of a frame
     */
    protected void collide(PhysicalEntity collidingObject, double lambda){

    }


    /**
     * instantly move an object to a location, resulting in a teleport. Beware : it wont check collision.
     * @param location
     */
    public void moveTo(GPoint2D location){
        if(grid.isInsideGrid(location) && grid.isInsideGrid(this.position)){
            try {
                grid.moveTo(this, location);
            } catch (OutOfBoundsException e) {
                e.printStackTrace();
            } catch (CantFindChunkException e) {
                e.printStackTrace();
            }
        }

    }


    /**
     * Function used to test if two objects are colliding, via their hitboxes
     * @param object
     * @return
     */
    public boolean isColliding(PhysicalEntity object){
        return isCollidable() && object.isCollidable() && this.hitbox.isColliding(object.getHitbox());
    }


    /**
     * Is used to set the speed of the object ( the norm only, and not it's direction. Use the move function instead )
     * @param speed a positive speed. If speed is negative, 0 is assigned to the object's speed
     */
    public void setSpeed(double speed){
        this.speed = (speed >= 0 && isMovable())?speed:0;
    }


    /**
     *
     * @return the hitbox of this object. Will return null if the object is not collidable ( you can test this with isCollidable() )
     */
    public Hitbox getHitbox(){
        return this.hitbox;
    }


    /**
     *
     * @return the position of this object
     */
    public GPoint2D getPhysicalPosition() {
        return this.position;
    }


    /**
     * Used to know if an object can collide
     * @return
     */
    public boolean isCollidable(){
        return this.collidable;
    }

    /**
     * Used to know if an object can move
     * @return
     */
    public boolean isMovable(){
        return this.movable;
    }

    /**
     * Set the collision table of this object's hitbox
     * @param table
     */
    public void setCollisionTable(CollisionTable table){
        hitbox.collisionTable = table;
    }

    /**
     * set a reference to the grid containing this object. Should not be used outside World.
     * @param grid
     */
    protected void setGrid(Grid grid){
        this.grid = grid;
    }

    protected void setCollidedLastFrame(boolean collidedLastFrame){
        this.collidedLastFrame = collidedLastFrame;
    }

    protected boolean hasCollidedLastFrame(){
        return this.collidedLastFrame;
    }

    protected GPoint2D getLastPhysicalPosition(){
        return lastPhysicalPosition;
    }

    protected void setLastPhysicalPosition(GPoint2D pos){
        lastPhysicalPosition = new GPoint2D(pos.getX(), pos.getY());
    }



    /**
     * Crenaud3 Hitbox class. Please note that the Hitbox position attribute is the top left corner coordinates
     * Also, a hitbox position is the position of it's linked object.
     * @author Aethor
     */
    public class Hitbox {

        private double height;
        private double width;
        private CollisionTable collisionTable;


        /**
         * Hitbox main constructor
         * @param height the height of the hitbox. Has to be strictly positive, or an exception will be issued
         * @param width the width of the hitbox. Has to be strictly positive, or an exception will be issued
         * @param collisionTable
         */
        private Hitbox(double height, double width, CollisionTable collisionTable){

            if (height > 0 && width > 0){
                this.height = height;
                this.width = width;
            }else{
                System.out.println("crenaud3.physics : Warning : attempting to create hitbox with negative width and/or height." +
                        "Wrong coordinates have been replaced by 1");
                this.width = (width > 0)?width:1;
                this.height = (height > 0)?height:1;
            }
            this.collisionTable = collisionTable;

        }


        /**
         * compare two Hitbox objects
         * @param h a Hitbox object
         * @return a boolean : true if the height and width of the two Hitboxes are equals ( beware : position does not matter ), false otherwise
         */
        public boolean equals(Hitbox h) {
            return (height == h.height && width == h.width);
        }


        /**
         * @return a string describing the current Hitbox object
         */
        public String toString(){
            return "Hitbox : (" + getX() + "," + getY() + ")\n\twidth : " + width + "\n\theight : " + height + "\n";
        }


        /**
         * check if two hitboxes are colliding, geometrically only
         * @param h a Hitbox object
         * @return a boolean : true if the two hitboxes are colliding ( geometrically ), false otherwise
         */
        public boolean isColliding(Hitbox h){
            return (getX() < h.getXMax()
                    && getXMax() > h.getX()
                    && getY() < h.getYMax()
                    && getYMax() > h.getY())
                    && (collisionTable.isColliding(h.collisionTable))
                    && (this != h);
        }


        /**
         * return the first coordinate of a Hitbox.
         * @return a double
         */
        public Double getX(){
            return PhysicalEntity.this.getPhysicalPosition().getX();
        }


        /**
         * return the second coordinate of a Hitbox.
         * @return a double
         */
        public Double getY(){
            return PhysicalEntity.this.getPhysicalPosition().getY();
        }


        /**
         * return the first coordinate of a Hitbox plus it's width.
         * @return a double
         */
        public Double getXMax(){
            return this.getX() + width;
        }


        /**
         * return the second coordinate of a Hitbox plus it's weight.
         * @return a double
         */
        public Double getYMax(){
            return this.getY() + height;
        }


        /**
         *
         * @return The point representing the center of the hitbox.
         */
        public GPoint2D getCenter() {
            return new GPoint2D(getX() + width/2, getY() + height/2);
        }

    }
}
