# crenaud3.physics

Welcome to crenaud3 physical motor.

## principles

crenaud3.physics main structure is a World. It is intended to contain a Grid, itself containing entities ( physical or not ).

To update your physical world, call World.update(double) each frame, specifying the duration of your frame in second. Typically, every entity will move.
Then, the motor will check for collisions and resolve them.

You can specify collisions layers and masks. For more details, check Godot's documentation ( the implementation is pretty similar ) on the subject. By default,
each object will collide with every other objects.

The PhysicalEntity class is meant to be extended. Specifically, the *customUpdate(double)* and *customCollision(double, PhysicalEntity)* functions were
created for this purpose : override them as you please.


## changelog

### 0.1c

* collision bugfix


### 0.1b

* better link with Grid
* entities will now check if their next move is authorized by the grid before moving
* hitbox's position are now linked with their respective objects

