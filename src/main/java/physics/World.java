package physics;

import datastructures.Entity;
import datastructures.Grid;
import datastructures.exception.CantFindChunkException;
import datastructures.exception.GridDimensionsException;
import datastructures.exception.OutOfBoundsException;

import java.util.LinkedList;

/**
 * Crenaud3 physical world class. Is meant to be updated by a controller every frame.
 * @author Aethor
 */
public class World {

    private static World worldInstance = null;
    private Grid grid;
    private LinkedList<Entity> entitiesToAdd;
    private LinkedList<Entity> entitiesToRemove;
    private boolean debug = false;


    /**
     * Create a new World ( to use in place of a constructor as World is a singleton ) and returns a reference to it.
     * The grid of the world will be empty.
     * @param x the x dimension of the game grid contained in the world
     * @param y the y dimension of the game grid contained in the world
     * @return
     */
    public static World getInstance(int x, int y){
        if(worldInstance == null)
            worldInstance = new World(x, y);
        return worldInstance;
    }

    /**
     * Create a new World ( to use in place of a constructor as World is a singleton ) and returns a reference to it.
     * The grid of the world will be empty.
     * @param x the x dimension of the game grid contained in the world
     * @param y the y dimension of the game grid contained in the world
     * @param chunking nbr used by the grid to define the number of chuncks
     * @return
     */
    public static World getInstance(int x, int y,int chunking){
        if(worldInstance == null)
            worldInstance = new World(x, y,chunking);
        return worldInstance;
    }

    /**
     * Create a new World ( to use in place of a constructor as World is a singleton ) and returns a reference to it.
     * @param x the x dimension of the game grid contained in the world
     * @param y the y dimension of the game grid contained in the world
     * @param list a list of entity to add directly to the world
     */
    public static World getInstance(int x, int y, LinkedList<PhysicalEntity> list){
        if(worldInstance == null)
            worldInstance = new World(x, y, list);
        return worldInstance;
    }

    /**
     * get the World singleton. Beware : if it's not initialized, it will return null.
     * @return A reference to the actual World instance or null
     */
    public static World getInstance(){
        return worldInstance;
    }

    /**
     * Constructor for a physical world. The grid of the world will be empty at creation.
     * @param x the x dimension of the game grid contained in the world
     * @param y the y dimension of the game grid contained in the world
     */
    private World(int x, int y){
        entitiesToRemove = new LinkedList<>();
        entitiesToAdd = new LinkedList<>();
        try {
            grid = new Grid(x, y);
        } catch (GridDimensionsException e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructor for a physical world. The grid of the world will be empty at creation.
     * @param x the x dimension of the game grid contained in the world
     * @param y the y dimension of the game grid contained in the world
     * @param chuncking the grid var about the number of chunk
     */
    private World(int x,int y,int chuncking){
        entitiesToRemove = new LinkedList<>();
        entitiesToAdd = new LinkedList<>();
        try {
            grid = new Grid(x, y, chuncking);
        } catch (GridDimensionsException e) {
            e.printStackTrace();
        }

    }


    /**
     * Constructor for a physical world.
     * @param x the x dimension of the game grid contained in the world
     * @param y the y dimension of the game grid contained in the world
     * @param list a list of entity to add directly to the world
     */
    private World(int x, int y, LinkedList<PhysicalEntity> list){
        this(x, y);
        for(PhysicalEntity entity : list){
            try {
                grid.add(entity);
                entity.setGrid(grid);
            } catch (OutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }




    /**
     * Should be called every frame by a controller. Update status ( position for example ) of every objects in the world list
     * @param lambda the duration of the frame in seconds
     */
    public void update(double lambda){

        if(debug)
            System.out.println("world : starting update with frame duration " + lambda + "s.");

        //Add and remove entities to the game grid
        entitiesToAdd.forEach(entity -> {
            this.add(entity);
        });
        entitiesToAdd = new LinkedList<>();

        entitiesToRemove.forEach(entity -> {
            this.remove(entity);
        });
        entitiesToRemove = new LinkedList<>();



        for(Entity object : grid.getEntities()){
            if(object instanceof PhysicalEntity){
                ((PhysicalEntity) object).setLastPhysicalPosition(((PhysicalEntity) object).getPhysicalPosition());
                ((PhysicalEntity) object).update(lambda);
            }
        }

        for(Entity object : grid.getEntities()){
            if(object instanceof PhysicalEntity && ((PhysicalEntity) object).isCollidable()){
                for(Entity otherObject : grid.getEntities()){
                    if(otherObject instanceof PhysicalEntity && ((PhysicalEntity) object).isColliding((PhysicalEntity) otherObject)){
                        ((PhysicalEntity) object).collide((PhysicalEntity) otherObject, lambda);
                        ((PhysicalEntity) object).setCollidedLastFrame(true);
                    }
                }
            }
        }

        for(Entity object : grid.getEntities()){
            if(object instanceof PhysicalEntity){
                if(((PhysicalEntity) object).hasCollidedLastFrame()){
                    ((PhysicalEntity) object).moveTo(((PhysicalEntity) object).getLastPhysicalPosition());
                    ((PhysicalEntity) object).setCollidedLastFrame(false);
                }
            }
        }

    }

    /**
     * Return the grid containing all entities in the physical world
     * @return a Grid object
     */
    public Grid getGrid() {
        return grid;
    }

    /**
     * Add an entity to the world.
     * @param entity
     */
    public void add(Entity entity){
        try {
            grid.add(entity);
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        if(entity instanceof PhysicalEntity)
            ((PhysicalEntity) entity).setGrid(grid);
    }

    /**
     * Remove an entity from the world.
     * @param entity
     */
    public void remove(Entity entity){
        try {
            grid.remove(entity);
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        } catch (CantFindChunkException e) {
            e.printStackTrace();
        }
        if(entity instanceof PhysicalEntity)
            ((PhysicalEntity) entity).setGrid(null);
    }

    /**
     * Add an entity to the world safely ( next frame ). To use when you want to add an entity to the world within it's main loop.
     * @param entity
     */
    public void delayedAdd(Entity entity){
        entitiesToAdd.add(entity);
    }

    /**
     * Add an entity to the world safely ( next frame ). To use when you want to add an entity to the world within it's main loop.
     * @param entity
     */
    public void delayedRemove(Entity entity){
        entitiesToRemove.add(entity);
    }


    /**
     * set the debug world variable
     * @param debug if set to true, World will print debug messages when updating
     */
    public void setDebug(boolean debug){
        this.debug = debug;
    }

}
