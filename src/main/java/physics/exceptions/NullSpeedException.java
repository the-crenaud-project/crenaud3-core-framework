package physics.exceptions;

/**
 * Exception fired when attempting to move an object with no speed
 */
public class NullSpeedException extends Exception {
    public NullSpeedException(String e){
        super(e);
    }
}
