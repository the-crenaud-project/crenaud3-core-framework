package datastructures;

import datastructures.exception.CantFindChunkException;
import datastructures.exception.GridDimensionsException;
import datastructures.exception.OutOfBoundsException;
import graphicalengine.Drawable;

import java.util.*;

/**
 * The Grid is an object to store all the entity of the game
 *
 * @author KLIPFEL Arthur
 */

public class Grid {
    private Chunk[][] matrix;
    private TreeMap<GPoint2D,GPoint2D> chunkCoords;
    protected double[][] transformationMatrix;
    protected double[][] invertedTransformationMatrix;
    private TreeMap<Integer,Entity> entities;
    protected Comparator<GPoint2D> pointComparator;
    protected Comparator<Entity> entityComparator;

    /**
     * Create a grid.
     * The dimension of the grid will be (width*Chunk.width)x(height*Chunk.width)
     * The the valid coordinates are inside [0;width*Chunk.width[ X [0;height*Chunk.width[
     *
     * Inside the grid, the entity can be find by id or by position
     *
     * @param width the width of the grid
     * @param height the height of the grid
     * @throws GridDimensionsException the width and the height need to be greater than 0
     */
    public Grid(float width,float height) throws GridDimensionsException {
        setup(width,height,10);
    }

    /**
     * Create a grid.
     * The dimension of the grid will be (width*Chunk.width)x(height*Chunk.width)
     * The the valid coordinates are inside [0;width*Chunk.width[ X [0;height*Chunk.width[
     *
     * Inside the grid, the entity can be find by id or by position
     *
     * @param width the width of the grid
     * @param height the height of the grid
     * @param nbChunkSqrt the square root of the number the desiered chunk, it's about optimisation so don't touch it if you don't know what you do
     * @throws GridDimensionsException the width and the height need to be greater than 0
     */
    public Grid(float width,float height,int nbChunkSqrt) throws GridDimensionsException {
        setup(width,height,nbChunkSqrt);
    }

    /**
     * setup the grid.
     * The dimension of the grid will be (width*Chunk.width)x(height*Chunk.width)
     * The the valid coordinates are inside [0;width*Chunk.width[ X [0;height*Chunk.width[
     *
     * @param width the width of the grid
     * @param height the height of the grid
     * @throws GridDimensionsException the width and the height need to be greater than 0
     */
    private void setup(float width,float height,int nbChunkSqrt) throws GridDimensionsException {
        this.entities=new TreeMap<Integer,Entity>();
        this.transformationMatrix=new double[][]{{1,0,0},{0,1,0}};//init transformation matrix with identity (no rotation) plus an offset
        this.invertedTransformationMatrix=new double[][]{{1,0,0},{0,1,0}};//init inverted transformation matrix with identity (no rotation) and no offset
        this.pointComparator=new Comparator<GPoint2D>() {
            public int compare(GPoint2D p1, GPoint2D p2) {
                if(p1.getY()<p2.getY())
                    return -1;
                if(p1.getY()>p2.getY())
                    return 1;
                if(p1.getX()<p2.getX())
                    return -1;
                if(p1.getX()>p2.getX())
                    return 1;
                return 0;
            }
        };
        this.chunkCoords=new TreeMap<>(this.pointComparator);
        for(Map.Entry<GPoint2D, GPoint2D> entry : this.chunkCoords.entrySet()){
            System.out.println(entry.getValue());
        }
        this.entityComparator=new Comparator<Entity>() {
            public int compare(Entity e1, Entity e2) {
                int res=pointComparator.compare(e1.getTransformedPos(),e2.getTransformedPos());
                if(res!=0)
                    return res;
                if(e1.getId()<e2.getId())
                    return -1;
                if(e1.getId()>e2.getId())
                    return 1;
                return 0;
            }
        };
        if (width <= 0 || height <= 0) {
            throw new GridDimensionsException("width and height of the Grid must be greater then 0 (width=" + width + ",height=" + height + ")");
        }
        float chunkScale=(float)Math.sqrt(width*height/nbChunkSqrt);
        Chunk.setWeight(chunkScale);
        int w=(int)Math.ceil(width/chunkScale),h=(int)Math.ceil(height/chunkScale);
        this.matrix = new Chunk[w][h];
        for (int x = 0; x < this.matrix.length; x++) {
            for (int y = 0; y < this.matrix[0].length; y++) {
                try {
                    this.matrix[x][y] = new Chunk(x, y);
                } catch (OutOfBoundsException e) {
                    e.printStackTrace();
                }
                this.chunkCoords.put(new GPoint2D(x,y),new GPoint2D(x,y));
            }
        }

    }

    /**
     * A fonction to apply a [2;3] transformation matrix to a 2d point
     *
     * @param transformation the transformation matrix
     * @param p input point
     * @return a transformed point
     */
    public static GPoint2D matmul(double[][] transformation,GPoint2D p){
        return new GPoint2D(p.getX()*transformation[0][0]+p.getY()*transformation[0][1]+transformation[0][2],p.getX()*transformation[1][0]+p.getY()*transformation[1][1]+transformation[1][2]);
    }

    /**
     * update the transformation matrix to rotate, translate and scale the map
     *
     * @param angle absolute angle of the rotation applied to the grid
     * @param yScale scale the y axis
     */
    public void setTransformation(float angle,float yScale){
        double sinA=Math.sin(angle),cosA=Math.cos(angle),width=this.matrix.length*Chunk.width,height=this.matrix[0].length*Chunk.width,minX=Double.POSITIVE_INFINITY,minY=Double.POSITIVE_INFINITY;
        double[][] newCorners=new double[4][2];
        this.transformationMatrix[0][0]=cosA;//rotation matrix
        this.transformationMatrix[0][1]=-sinA;
        this.transformationMatrix[1][0]=sinA;
        this.transformationMatrix[1][1]=cosA;
        newCorners[0][0]=0;//calculate new position of the corner
        newCorners[0][1]=0;
        newCorners[1][0]=width*this.transformationMatrix[0][0];
        newCorners[1][1]=width*this.transformationMatrix[1][0];
        newCorners[2][0]=width*this.transformationMatrix[0][0]+height*this.transformationMatrix[0][1];
        newCorners[2][1]=width*this.transformationMatrix[1][0]+height*this.transformationMatrix[1][1];
        newCorners[3][0]=height*this.transformationMatrix[0][1];
        newCorners[3][1]=height*this.transformationMatrix[1][1];
        for(int i=0;i<4;i++){//get
            if(newCorners[i][0]<minX)
                minX=newCorners[i][0];
            if(newCorners[i][1]<minY)
                minY=newCorners[i][1];
        }
        this.transformationMatrix[0][2]=-minX;
        this.transformationMatrix[1][0]=yScale*sinA;
        this.transformationMatrix[1][1]=yScale*cosA;
        this.transformationMatrix[1][2]=-yScale*minY;
        //calculate the inverted matrix
        this.invertedTransformationMatrix[0][0]=cosA;
        this.invertedTransformationMatrix[0][1]=sinA/yScale;
        this.invertedTransformationMatrix[0][2]=minX*cosA+minY*sinA;
        this.invertedTransformationMatrix[1][0]=-sinA;
        this.invertedTransformationMatrix[1][1]=cosA/yScale;
        this.invertedTransformationMatrix[1][2]=-minX*sinA+minY*cosA;

        TreeMap<GPoint2D,GPoint2D> tmp=new TreeMap<>(this.pointComparator);
        for(Map.Entry<GPoint2D, GPoint2D> entry : this.chunkCoords.entrySet()){
            tmp.put(matmul(this.transformationMatrix,entry.getValue()),entry.getValue());
        }
        this.chunkCoords=tmp;
    }

    /**
     * A function to get the coordinates of the points in the grid in the transformed bases from the grid bases
     *
     * @param p the point in the transformed space
     * @return the position of the point in the transformed base
     */

    public GPoint2D getTransformedCoordinates(GPoint2D p){
        return Grid.matmul(this.transformationMatrix,p);
    }

    /**
     * A function to get the coordinates of the points in the grid in the grid bases from the transformed bases
     *
     * @param p the point in the transformed space
     * @return the position of the point in the grid base
     */

    public GPoint2D getGridCoordinates(GPoint2D p){
        return Grid.matmul(this.invertedTransformationMatrix,p);
    }

    /**
     * A function to check if a coordinate is inside the grid or not
     *
     * @param p the coordinates of the position
     * @return true if it's inside, false in other case
     */
    public boolean isInsideGrid(GPoint2D p){
        GPoint2D coords=new GPoint2D((int)(p.getX()/Chunk.width),(int)(p.getY()/Chunk.width));
        if(p.getX()<0 || coords.getX()>=this.matrix.length || p.getY()<0 || coords.getY()>=this.matrix[0].length){
            return false;
        }
        return true;
    }

    /**
     * A function the get the coordinate of a chunk of a certain position
     *
     * @param p the coordinates of the position
     * @return the coordinates of the chunk
     * @throws OutOfBoundsException the point need the be INSIDE the grid
     */
    protected GPoint2D getChunkCoords(GPoint2D p) throws OutOfBoundsException{
        GPoint2D coords=new GPoint2D((int)(p.getX()/Chunk.width),(int)(p.getY()/Chunk.width));
        if(p.getX()<0 || coords.getX()>=this.matrix.length || p.getY()<0 || coords.getY()>=this.matrix[0].length){
            throw new OutOfBoundsException("you try to get a chunk outside the grid, position must be between 0.0 and "+(Chunk.width*this.matrix.length)+" for x and between 0.0 and "+(Chunk.width*this.matrix[0].length)+" for y '(x="+p.getX()+",y="+p.getY()+")");
        }
        return coords;
    }

    /**
     * A function to add an entity at a certain location of the grid
     *
     * @param pos the position of the entity
     * @param e the entity to add
     * @throws OutOfBoundsException the entity need to be add INSIDE the grid
     */
    public void add(GPoint2D pos, Entity e) throws OutOfBoundsException {
        e.position.setLocation(pos);
        this.add(e);
    }

    /**
     * A function to add an entity at this position on the grid
     *
     * @param e the entity to add on the grid
     * @throws OutOfBoundsException the entity need to be add INSIDE the grid
     */
    public void add(Entity e) throws OutOfBoundsException {
        GPoint2D chunk=this.getChunkCoords(e.position);
        this.matrix[(int)chunk.getX()][(int)chunk.getY()].add(e);
        this.entities.put(e.getId(),e);
    }

    /**
     * A function to move an entity from is position to another
     *
     * @param e the entity to move
     * @param newPos the new location of the entity
     * @throws OutOfBoundsException the entity can't move outside the grid
     * @throws CantFindChunkException the entity need to be on the right chunk
     */
    public void moveTo(Entity e,GPoint2D newPos) throws OutOfBoundsException, CantFindChunkException {
        GPoint2D chunk=this.getChunkCoords(newPos);
        GPoint2D prevChunk=this.getChunkCoords(e.position);
        if(prevChunk!=chunk){//different chunk
            if(!this.matrix[(int)prevChunk.getX()][(int)prevChunk.getY()].remove(e)){
                throw new CantFindChunkException("entity id="+e.getId()+" isn't on the right chunk");
            }
            e.position.setLocation(newPos);
            this.matrix[(int)chunk.getX()][(int)chunk.getY()].add(e);
        }else{//same chunk
            e.position.setLocation(newPos);
        }
    }

    /**
     * A function to remove an entity from the grid
     *
     * @param e the entity to remove
     * @throws OutOfBoundsException the entity need to be inside the grid
     * @throws CantFindChunkException then entity need to be in the right chunk
     */
    public void remove(Entity e) throws OutOfBoundsException, CantFindChunkException{
        GPoint2D chunk=this.getChunkCoords(e.position);
        if(!this.matrix[(int)chunk.getX()][(int)chunk.getY()].remove(e)){
            throw new CantFindChunkException("entity id="+e.getId()+" isn't on the right chunk or doesn't exist");
        }
        this.entities.remove(e.getId());
    }

    /**
     * get an entity by id
     *
     * @param id the id of the entity
     * @return the entity (or null if there is not entity with such id)
     */
    public Entity get(int id){
        return this.entities.get(id);
    }

    /**
     * Get an entity by location.
     * If there is more than one entity the this location, the function will pick one randomly
     *
     * @param pos the location of the entity
     * @return return the entity (or null if there is not entity with such position)
     * @throws OutOfBoundsException
     * @throws CantFindChunkException
     */
    public Entity get(GPoint2D pos) throws OutOfBoundsException, CantFindChunkException{
        GPoint2D chunk=this.getChunkCoords(pos);
        Entity e=this.matrix[(int)chunk.getX()][(int)chunk.getY()].get(pos);
        if(e==null){
            throw new CantFindChunkException("entity id="+e.getId()+" isn't on the right chunk or doesn't exist");
        }
        return e;
    }

    /**
     * A function to sort the entity of all chunk by distance to display them in the right order
     */
    public void sort(){//multi-threading???
        for(int x=0;x<this.matrix.length;x++){
            for(int y=0;y<this.matrix[0].length;y++){
                this.matrix[x][y].sort(this.entityComparator);
            }
        }
    }

    /**
     * Get all the drawable entity of the grid
     *
     * Be careful, this function will always sort object even if they are already sorted
     *
     * @return a linked list of sorted drawable object
     */
    public LinkedList<Drawable> getDrawables(){
        return (LinkedList<Drawable>)(LinkedList<?>) this.getDrawables(true);
    }

    /**
     * Get all the drawable entity of the grid
     *
     * The drawable can be sorted with sort=true or not
     *
     * @param sort enable or disable sorting
     * @return a linked list of sorted drawable object
     */
    public LinkedList<Entity> getDrawables(boolean sort){
        LinkedList<Entity> lst = new LinkedList<Entity>();
        for(Map.Entry<Integer, Entity> entry : this.entities.entrySet()){//transform before sort
            entry.getValue().TransformedPosition=matmul(this.transformationMatrix,entry.getValue().position);
        }
        if(sort){
            this.sort();
        }
        for(Map.Entry<GPoint2D, GPoint2D> entry : this.chunkCoords.entrySet()){//much more simple solution
            //System.out.println(entry.getKey()+" => "+entry.getValue());
            lst.addAll(this.matrix[(int)entry.getValue().getX()][(int)entry.getValue().getY()].getDrawables());
        }

        /*for(int x=0;x<diag;x++){//some ISO 3D madness (start from the farest corner to the closest corner)
            if(this.matrix[0].length>x) {
                lenDiag = x + 1;
                if(lenDiag>=this.matrix.length){
                    lenDiag=this.matrix.length-1;
                }
                for (int y = 0; y < lenDiag; y++) {
                    //System.out.println(""+y+";"+((this.matrix[0].length - x -1)+y));
                    lst.addAll(this.matrix[y][(this.matrix[0].length - x -1)+y].getDrawables());
                }
            }
            else{
                lenDiag = (diag - x);
                if(lenDiag>=this.matrix[0].length){
                    lenDiag=this.matrix[0].length-1;
                }
                for (int y = 0; y < lenDiag; y++) {
                    //System.out.println(""+((x-this.matrix[0].length+1)+y)+";"+y);
                    lst.addAll(this.matrix[(x-this.matrix[0].length+1)+y][y].getDrawables());
                }
            }
        }
        for(Entity e : lst){
            e.TransformedPosition.setLocation(Grid.matmul(this.transformationMatrix,e.position));
        }*/
        return  lst;
    }

    /**
     * A function to get all the entities of the grid
     * @return
     */
    public LinkedList<Entity> getEntities(){
        return new LinkedList<Entity>(this.entities.values());
    }

    /**
     * A function to get the closest entities of a location.
     * The entity can be in a different chunk, it will always work
     *
     * Be careful, this function is not very fast, use little maxDist to save time
     *
     * @param p the coordinates of the location
     * @param maxDist the max distance between the location and the entity
     * @return the closest entity if there is on closer than maxDist
     */
    public Entity getClosest(GPoint2D p,double maxDist){
        double minX=p.getX()-maxDist,minY=p.getY()-maxDist,maxX=p.getX()+maxDist,maxY=p.getY()+maxDist,currentDist,min=Double.POSITIVE_INFINITY;//define the square where we search the entity
        Entity closest=null,current;
        if(minX<0){//check if the square fit in the grid, we don't need to search outside
            minX=0;
        }
        if(minY<0){
            minY=0;
        }
        if(maxX>=(Chunk.width*this.matrix.length)){
            maxX=Chunk.width*this.matrix.length;
        }
        if(maxY>=(Chunk.width*this.matrix[0].length)){
            maxY=Chunk.width*this.matrix[0].length;
        }
        minX=Math.floor(minX/Chunk.width);//to get the coordinates of the chunks
        minY=Math.floor(minY/Chunk.width);
        maxX=Math.ceil(maxX/Chunk.width);
        maxY=Math.ceil(maxY/Chunk.width);
        for(int x=(int)minX;x<maxX;x++){//search in all the chunks
            for(int y=(int)minY;y<maxY;y++){
                current=this.matrix[x][y].getClosest(p,maxDist);
                if(current!=null){
                    currentDist=p.distance(current.position);
                    if(currentDist<min){
                        min=currentDist;
                        closest=current;
                    }
                }
            }
        }
        return closest;
    }

    /**
     * A function to check if all the entity are on the right chunk.
     * Just for debugging
     *
     * @return true if all the entity are on the right chunk
     */
    public boolean isValid(){
        for(int x=0;x<this.matrix.length;x++){
            for(int y=0;y<this.matrix[0].length;y++) {
                if(!this.matrix[x][y].isValid()){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * A function to display all the entity on a chunk.
     * Just for debugging
     */
    public void debugLogs(){
        System.out.println("Grid:");
        for(int x=0;x<this.matrix.length;x++){
            for(int y=0;y<this.matrix[0].length;y++) {
                this.matrix[x][y].debugLogs();
            }
        }
        for(int y=0;y<2;y++){
            for(int x=0;x<3;x++){
                float sum=x==2?(float)this.transformationMatrix[y][2]:0;
                for(int i=0;i<2;i++){
                    sum+=this.transformationMatrix[y][i]*this.invertedTransformationMatrix[i][x];
                }
                System.out.printf("%.2f  ", sum);
            }
            System.out.println();
        }
        for(int y=0;y<2;y++){
            for(int x=0;x<3;x++){
                System.out.print(this.transformationMatrix[y][x]+"   ");
            }
            System.out.println();
        }
        for(int y=0;y<2;y++){
            for(int x=0;x<3;x++){
                System.out.print(this.invertedTransformationMatrix[y][x]+"   ");
            }
            System.out.println();
        }
    }
}
