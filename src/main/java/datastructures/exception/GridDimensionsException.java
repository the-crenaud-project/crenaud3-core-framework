package datastructures.exception;

/**
 * custom exception for checking if the dimension of the grid is valid
 *
 * @author KLIPFEL Arthur
 */

public class GridDimensionsException extends Exception {
    public GridDimensionsException(String s) {
        super(s);
    }
}
