package datastructures.exception;

/**
 * custom exception for checking to coordinates of the entity inside a chunk or the grid
 *
 * @author KLIPFEL Arthur
 */

public class OutOfBoundsException extends Exception {
    public OutOfBoundsException(String s) {
        super(s);
    }
}
