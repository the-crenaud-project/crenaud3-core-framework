package datastructures.exception;

/**
 * custom exception for checking if the entity is inside the right chunk
 *
 * @author KLIPFEL Arthur
 */

public class CantFindChunkException extends Exception {
    public CantFindChunkException(String s) {
        super(s);
    }
}
