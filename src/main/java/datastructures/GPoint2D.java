package datastructures;

import javafx.geometry.Point2D;

/**
 * Crenaud3's own Point class
 * @author Aethor
 */
public class GPoint2D{

    private double x;
    private double y;

    /**
     * default constructor for a GPoint2D
     * both coordinates will be set to 0 by default
     */
    public GPoint2D(){
        this.x = 0;
        this.y = 0;
    }


    /**
     *
     * @param x
     * @param y
     */
    public GPoint2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public GPoint2D(Point2D point){
        this.x = point.getX();
        this.y = point.getY();
    }

    public void setLocation(GPoint2D point){
        this.x = point.x;
        this.y = point.y;
    }

    public void setLocation(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double distance(GPoint2D point){
        return Math.sqrt((x - point.x) * (x - point.x) + (y - point.y) * (y - point.y));
    }

    public double getX(){
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public String toString() {
        return ("GPoint2D : (" + x + "," + y + ")\n");
    }

    public Point2D toPoint2D(){
        return new Point2D(x, y);
    }


    /**
     * Used to convert a javafx Point2D to a Gpoint2D
     * @param point
     * @return
     */
    public static GPoint2D toGpoint2D(Point2D point){
        return new GPoint2D(point.getX(), point.getY());
    }

}
