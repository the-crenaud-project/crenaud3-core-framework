package datastructures;

import datastructures.exception.OutOfBoundsException;

import java.util.*;
import java.util.List;

/**
 * Chunk is a class to store Entity on a little portion of the grid.
 * A Chunk is an element of the matrix inside the grid.
 *
 * @author KLIPFEL Arthur
 */

public class Chunk {
    protected LinkedList<Entity> entities;
    //protected Comparator<Entity> entityComparator;
    protected GPoint2D coordinates;
    protected static float width=1.0f;

    /**
     * Create a chunk of the grid.
     *
     * @param x the coordinate of the chunk on the grid
     * @param y the coordinate of the chunk on the grid
     * @throws OutOfBoundsException the coordinates need to be greater or equals to 0
     */
    public Chunk(int x,int y) throws OutOfBoundsException {
        /*this.entityComparator=new Comparator<Entity>() {
            public int compare(Entity e1, Entity e2) {
                double y1=e1.getY()-e1.getX(),y2=e2.getY()-e2.getX();//changement de base
                double x1=e1.getX()+e1.getY(),x2=e2.getX()+e2.getY();
                if(y1<y2)
                    return -1;
                if(y1>y2)
                    return 1;
                if(x1<x2)
                    return -1;
                if(x1>x2)
                    return 1;
                if(e1.getId()<e2.getId())//doesn't blink if 2 entity are superimposed (each time the same on the top)
                    return -1;
                if(e1.getId()>e2.getId())
                    return 1;
                return 0;
            }
        };*/
        this.entities=new LinkedList<Entity>();
        if(x<0 || y<0){
            throw new OutOfBoundsException("the coordinates of a chunk must be greater or equals to 0 (x="+x+",y="+y+")");
        }
        this.coordinates=new GPoint2D(x,y);
    }

    /**
     * A function to change the chunk width
     *
     * @param weight the new weight of the chunk
     */
    protected static void setWeight(float weight){
        Chunk.width=weight>0?weight:1.0f;
    }

    /**
     * A function to get an entity on a certain location.
     *
     * @param p the coordinate of the entity
     * @return an entity
     */
    public Entity get(GPoint2D p){
        for(Entity e : this.entities){
            if(e.getX()==p.getX() && e.getY()==p.getY()) {
                return e;
            }
        }
        return null;
    }

    /**
     * A function to add an entity on the chunk.
     *
     * @param e the entity to add
     * @throws OutOfBoundsException the coordinates of the entity need to be ON the chunk
     */
    public void add(Entity e) throws OutOfBoundsException{
        GPoint2D pos=e.position;
        if(pos.getX()<this.coordinates.getX()*this.width || pos.getX()>=(this.coordinates.getX()+1)*this.width || pos.getY()<this.coordinates.getY()*this.width || pos.getY()>=(this.coordinates.getY()+1)*this.width)
            throw new OutOfBoundsException("the position of an entity in a chunk must be between "+(this.coordinates.getX()*this.width)+" and "+((this.coordinates.getX()+1)*this.width)+" for x and between "+(this.coordinates.getY()*this.width)+" and "+((this.coordinates.getY()+1)*this.width)+" for y (x="+pos.getX()+",y="+pos.getY()+",id="+e.getId()+")");
        this.entities.add(e);
    }

    /**
     * A function to remove an entity from a chunk.
     *
     * @param e the entity to remove
     * @return true if the entity is removed well
     */
    public boolean remove(Entity e){//may be more efficient?
        for(Iterator<Entity> i = this.entities.iterator(); i.hasNext();) {
            Entity data = i.next();
            if (data.getId() == e.getId()) {
                i.remove();
                return true;//some dirty things
            }
        }
        return false;
    }

    /**
     * A function to sort the entities on the chunk.
     * The closest Entity will be at the end of the list.
     */
    public void sort(Comparator<Entity> cmp){
        Collections.sort(this.entities, cmp);
    }

    /**
     * A Function to get all the drawable element of a chunk
     * This function can sort the element to display it in the right order (from the farest to the closest)
     * One's the chunk is sorted, it stay sorted until an entity move
     *
     * @param sort enable or disable sorting before build the list
     * @return a list of DrawableEntity element
     */
    public List<Entity> getDrawables(boolean sort,Comparator<Entity> cmp){
        List<Entity> lst = new LinkedList<Entity>();
        if(sort) {
            this.sort(cmp);
        }
        for(Entity e : this.entities){
            if(e.isDrawable()) {
                lst.add(e);
            }
        }
        return lst;
    }
    /**
     * A Function to get all the drawable element of a chunk
     *
     * @return a list of DrawableEntity element
     */
    public List<Entity> getDrawables(){
        return getDrawables(false,null);
    }

    /**
     * A function to get all the entity on a chunk
     *
     * @return a list of all the entities of the chunk
     */
    public List<Entity> getEntity(){
        return this.entities;
    }

    /**
     * A function to get the closest entity of a specific point (with a max distance)
     *
     * @param p the coordinates of the point
     * @param maxDist the max distance between the point and the closest entity
     * @return An entity if there is one closer then maxDist
     */
    public Entity getClosest(GPoint2D p,double maxDist){
        double current,min=Double.POSITIVE_INFINITY;
        Entity closest=null;
        for(Entity e : this.entities){
            current=e.position.distance(p);
            if(current<min && current<=maxDist){
                closest=e;
                min=current;
            }
        }
        return closest;
    }

    /**
     * A function to check if all the entity are on the right chunk.
     * Just for debugging
     *
     * @return true if all the entity are on the right chunk
     */
    public boolean isValid(){
        for(Entity e : this.entities){
            if(e.getX()<this.coordinates.getX()* width)
                return false;
            if(e.getX()>=(this.coordinates.getX()+1)* width)
                return false;
            if(e.getY()<this.coordinates.getY()* width)
                return false;
            if(e.getY()>=(this.coordinates.getY()+1)* width)
                return false;
        }
        return true;
    }

    /**
     * A function to display all the entity on a chunk.
     * Just for debugging
     */
    public void debugLogs(){
        System.out.println("    Chunk: (x="+this.coordinates.getX()+",y="+this.coordinates.getY()+")");
        for(Entity e : this.entities){
            System.out.println("    -Entity: (id="+e.getId()+",x="+e.getX()+",y="+e.getY()+")");
        }
    }
}
