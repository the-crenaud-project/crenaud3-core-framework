package datastructures;


/**
 * A class to put something on the grid, it can be display or not.
 *
 * Warning: use the Grid object to move an Entity
 *
 * @author KLIPFEL Arthur
 */

public class Entity {
    private static int count=0;
    private int id;
    protected boolean drawable;
    protected GPoint2D position;
    protected GPoint2D TransformedPosition;

    /**
     * Create a new Entity with a unique id
     * @param drawable
     */
    public Entity(boolean drawable){
        this.drawable=drawable;
        this.id=this.count++;
        this.position=new GPoint2D();
        this.TransformedPosition=new GPoint2D();
    }

    /**
     * Create a new Entity with an unique id
     * @param drawable
     * @param position
     */
    public Entity(boolean drawable, GPoint2D position){
        this.position = position;
        this.drawable = drawable;
        this.id = this.count++;
        this.TransformedPosition = new GPoint2D();
    }

    /**
     * Create a new Entity with a unique id
     */
    public Entity(){
        this.drawable=false;
        this.id=this.count++;
        this.position=new GPoint2D();
    }
    /**
     * @return true is the Entity is drawable
     */
    public boolean isDrawable(){
        return this.drawable;
    }
    /**
     * @return return the id of the Entity
     */
    public int getId(){
        return this.id;
    }
    /**
     * @return return the transfored postion of the entity
     */
    public GPoint2D getTransformedPos(){
        return TransformedPosition;
    }
    /**
     * @return return the x coordinate of the Entity
     */
    public double getX(){
        return this.position.getX();
    }
    /**
     * @return return the y coordinate of the Entity
     */
    public double getY(){
        return this.position.getY();
    }
}
