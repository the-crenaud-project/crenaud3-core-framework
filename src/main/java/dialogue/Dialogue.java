package dialogue;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import scenecontroller.Scene;
import scenecontroller.SceneController;

import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

public class Dialogue extends Scene {

    private DialogueManager manager;
    private MediaPlayer music;
    private MediaPlayer voice;
    private Timer dialogueDisplay;

    private DialogueStep current;
    private final int dialogueSpeed = 50;
    private ColorAdjust unhighlight;

    @FXML private ImageView leftCharacter;
    @FXML private ImageView rightCharacter;
    @FXML private ImageView background;
    @FXML private Label nameDisplay;
    @FXML private Label textDisplay;
    @FXML private Label codeLabel;
    @FXML private Button skipButton;

    /**
     * Constructor
     * Adds info about fxml and css paths
     */
    public Dialogue() {
        fxmlPath = "dialogue/fxml/dialogue.fxml";
        cssPath = "dialogue/css/dialogue.css";

    }

    /**
     * Method to extend. Should load everything from config
     */
    @Override
    public void play(SceneController controller, InputStream configFile) {
        super.play(controller, configFile);

        try {
            manager = new DialogueManager(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        rightCharacter.setScaleX(-1);
        unhighlight = new ColorAdjust();
        unhighlight.setBrightness(-0.5);

        codeLabel.setText("");

        nextDialogue();
    }

    /**
     * Called by the SceneController
     * Should close the window and unload everything
     */
    @Override
    public void end() {
        if (music != null)
            music.stop();
        if (voice != null)
            voice.stop();
        if (controller != null)
            controller = null;
        if (dialogueDisplay != null) dialogueDisplay.cancel();
    }

    /**
     * Display a character to the screen
     * @param current DialogueStep
     * @param actor Character to display
     */
    private void displayCharacter(DialogueStep current, DialogueCharacter actor){
        if (current.getCharacterPosition().equals("left")){
            leftCharacter.setImage(actor.getSprite(current.getCharacterBehavior()));
            leftCharacter.setEffect(null);
            if (current.isAlone()) rightCharacter.setImage(null);
            else rightCharacter.setEffect(unhighlight);
        } else if (current.getCharacterPosition().equals("right")){
            rightCharacter.setImage(actor.getSprite(current.getCharacterBehavior()));
            rightCharacter.setEffect(null);
            if (current.isAlone()) leftCharacter.setImage(null);
            else leftCharacter.setEffect(unhighlight);
        }
    }

    /**
     * Display the next dialogue
     */
    private void nextDialogue() {
        if (manager.hasNextDialogue()){
            current = manager.getNextDialogue();
            DialogueCharacter actor = null;

            if (manager.getCharacter(current.getCharacterId()) != null){

                actor = manager.getCharacter(current.getCharacterId());
                displayCharacter(current, actor);

                textDisplay.setText(String.valueOf(current.getText().charAt(0)));

                if (dialogueDisplay == null) dialogueDisplay = new Timer();
                else {
                    dialogueDisplay.cancel();
                    dialogueDisplay = new Timer();
                }

                dialogueDisplay.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(new Runnable() {
                            @Override
                                public void run() { textDisplay.setText(getNewDialogueString(dialogueDisplay)); }
                        });
                    }
                }, dialogueSpeed, dialogueSpeed);

                nameDisplay.setText(actor.getFirstName() + " " + actor.getLastName());

            }

            if (current.getBackground() >= 0 && manager.getBackground(current.getBackground()) != null)
                background.setImage(manager.getBackground(current.getBackground()));

            if (current.getMusic() >= 0 && manager.getMusic(current.getMusic()) != null){
                if (music != null)
                    music.stop();

                music = new MediaPlayer(manager.getMusic(current.getMusic()));
                music.setVolume(music.getVolume()/4);
                /* Auto repeat of music */
                music.setOnEndOfMedia(new Runnable() {
                    @Override
                    public void run() {
                        music.seek(Duration.ZERO);
                    }
                });
                music.play();
            }

            if (current.getSound() != null){
                if (voice != null)
                    voice.stop();

                voice = new MediaPlayer(current.getSound());
                voice.play();
            }

        } else {
            skipButton.setDisable(true);
            controller.sceneEnded();
        }
    }

    /**
     * Used to display the dialogue slowly
     * @param timer a Timer
     * @return String
     */
    private String getNewDialogueString(Timer timer){
        if (current.getText().equals(textDisplay.getText())){
            current.setFinished(true);
            timer.cancel();
            return current.getText();
        }
        else
            return textDisplay.getText() + current.getText().charAt(textDisplay.getText().length());
    }

    /**
     * Pass to the next dialogue
     * @param event ActionEvent
     */
    @FXML
    public void handleSkipAction(ActionEvent event) {
        if (current.isFinished())
            nextDialogue();
        else {
            textDisplay.setText(current.getText());
            current.setFinished(true);
        }
    }

    /**
     * Pass all dialogues
     * @param event ActionEvent
     */
    @FXML
    public void handleSkipAllAction(ActionEvent event){
        while (manager.hasNextDialogue()) manager.skipDialogue();
        nextDialogue();
    }

}
