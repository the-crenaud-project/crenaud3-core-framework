package dialogue;

import com.google.gson.stream.JsonReader;
import javafx.scene.media.Media;

import java.io.IOException;

/**
 * Describe a step of a dialogue
 * Contains ambient music, background, character mood, dialogue and sound
 * Package private
 * @author klmp200
 */
class DialogueStep {

    private int characterId;
    private int background = -1;
    private int music = -1;
    private String characterBehavior;
    private String characterPosition;
    private String text;
    private Media sound;
    private boolean isFinished;
    private boolean isAlone;

    /**
     * Generate a new dialogue step from a json object
     * @param reader JsonReader
     * @throws IOException if error reading the json
     */
    public DialogueStep(JsonReader reader) throws IOException {
        this.isFinished = false;
        this.isAlone = false;

        String name = null;
        reader.beginObject();
        while (reader.hasNext()){
            name = reader.nextName();
            switch (name) {
                case "character":
                    this.parseCharacter(reader);
                    break;
                case "text":
                    this.text = reader.nextString();
                    break;
                case "sound":
                    this.sound = new Media(getClass().getClassLoader().getResource(reader.nextString()).toExternalForm());
                    break;
                case "background":
                    this.background = reader.nextInt();
                    break;
                case "music":
                    this.music = reader.nextInt();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();

    }

    /**
     * Add information about the talking character from a json
     * @param reader a JsonReader
     * @throws IOException if malformed json
     */
    private void parseCharacter(JsonReader reader) throws IOException {
        String name;
        reader.beginObject();

        while (reader.hasNext()){
            name = reader.nextName();
            switch (name){
                case "id":
                    this.characterId = reader.nextInt();
                    break;
                case "behavior":
                    this.characterBehavior = reader.nextString();
                    break;
                case "position":
                    this.characterPosition = reader.nextString();
                    break;
                case "alone":
                    this.isAlone = reader.nextBoolean();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }

        reader.endObject();
    }

    /**
     * Get id of the current talking character
     * @return id of the character in the character list of the Dialogue
     */
    public int getCharacterId() {
        return characterId;
    }

    /**
     * Get id of the current background
     * @return id of the background in the background list of the Dialogue
     */
    public int getBackground() {
        return background;
    }

    /**
     * Get id of the current music
     * @return id of the music in the music list of the scene
     */
    public int getMusic() {
        return music;
    }

    /**
     * Get the behavior of the current character
     * @return key for the map of behavior of a character
     */
    public String getCharacterBehavior() {
        return characterBehavior;
    }

    /**
     * Get position of the character on the screen
     * @return either left or right
     */
    public String getCharacterPosition() {
        return characterPosition;
    }

    /**
     * Get the current dialogue text
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * Get the sound emitted by the character
     * @return sound to play
     */
    public Media getSound() {
        return sound;
    }

    /**
     * Look if the display of the dialogue is finished
     * @return boolean
     */
    public boolean isFinished() {
        return isFinished;
    }

    /**
     * Mark the dialogue as finished
     * @param finished a boolean
     */
    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    /**
     * Know if the character should be displayed alone
     * @return boolean
     */
    public boolean isAlone() {
        return isAlone;
    }
}
