package dialogue;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import javafx.scene.image.Image;
import javafx.scene.media.Media;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Builds the dialogue from a json
 * Package protected
 * @author klmp200
 */
class DialogueBuilder {

    private List<Image> backgrounds = new ArrayList<>();
    private List<Media> musics = new ArrayList<>();
    private List<DialogueCharacter> characters = new ArrayList<>();
    private List<DialogueStep> dialogueSteps = new ArrayList<>();

    /**
     * Build a new DialogueBuilder from a json
     * @param configFile as InputStream
     * @throws IOException if json malformed or incorrect file
     */
    public DialogueBuilder(InputStream configFile) throws IOException {
        Gson g = new Gson();
        JsonReader reader = g.newJsonReader(new InputStreamReader(configFile));
        String name = null;

        reader.beginObject();
        while (reader.hasNext()){
            name = reader.nextName();
            switch (name) {
                case "characters":
                    this.parseCharacters(reader);
                    break;
                case "backgrounds":
                    this.parseBackgrounds(reader);
                    break;
                case "dialogues":
                    this.parseDialogues(reader);
                    break;
                case "musics":
                    this.parseMusics(reader);
                    break;
                default:
                    reader.skipValue();
                    break;
            }

        }
        reader.endObject();
    }

    /**
     * Create a list of DialogueStep from a json array
     * @param reader as JsonReader
     * @throws IOException if json is malformed
     */
    private void parseDialogues(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext())
            this.dialogueSteps.add(new DialogueStep(reader));
        reader.endArray();
    }

    /**
     * Create a list of music from a json array
     * @param reader as JsonReader
     * @throws IOException if json is malformed
     */
    private void parseMusics(JsonReader reader) throws IOException {
        Media music = null;
        reader.beginArray();
        while (reader.hasNext()) {
            music = new Media(getClass().getClassLoader().getResource(reader.nextString()).toExternalForm());
            musics.add(music);
        }
        reader.endArray();
    }

    /**
     * Create a list of backgrounds from a json array
     * @param reader as JsonReader
     * @throws IOException if json is malformed
     */
    private void parseBackgrounds(JsonReader reader) throws IOException {
        reader.beginArray();
        Image img = null;
        while(reader.hasNext()){
            img = new Image(getClass().getResourceAsStream(reader.nextString()));
            this.backgrounds.add(img);
        }
        reader.endArray();
    }

    /**
     * Create a list of characters from a json array
     * @param reader as JsonReader
     * @throws IOException if json is malformed
     */
    private void parseCharacters(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext())
            this.characters.add(new DialogueCharacter(reader.nextString()));
        reader.endArray();
    }

    /**
     * Get the list of backgrounds
     * @return list of Image
     */
    public List<Image> getBackgrounds() {
        return backgrounds;
    }

    /**
     * Get the list of musics
     * @return list of Media
     */
    public List<Media> getMusics() {
        return musics;
    }

    /**
     * Get the list of characters
     * @return list of DialogueCharacter
     */
    public List<DialogueCharacter> getCharacters() {
        return characters;
    }

    /**
     * Get the list of dialogue step
     * @return list of DialogueStep
     */
    public List<DialogueStep> getDialogueSteps() {
        return dialogueSteps;
    }
}
