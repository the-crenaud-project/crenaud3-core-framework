package dialogue;

import javafx.scene.image.Image;

import java.io.InputStream;

/**
 * DialogueCharacter sprite for dialogue
 * Package private
 * @author klmp200
 */
class DialogueSprite {
    private InputStream imageStream;
    private Image img;

    /**
     * Get the image of the sprite
     * @return Image
     */
    public Image getImg() {
        return img;
    }

    /**
     * Generate a new dialogue sprite
     * @param path of the file
     */
    public DialogueSprite(String path) {
        this.imageStream = getClass().getResourceAsStream(path);
        this.img = new Image(imageStream);
    }

    /**
     * Generate a new dialogue sprite with custom parameters
     * @param path of the file
     * @param width of the sprite
     * @param height of the sprite
     * @param preserveRatio of the sprite
     * @param smooth to have a smooth sprite
     */
    public DialogueSprite(String path, int width, int height, boolean preserveRatio, boolean smooth){
        this.imageStream = getClass().getResourceAsStream(path);
        this.img = new Image(imageStream, width, height, preserveRatio, smooth);
    }
}
