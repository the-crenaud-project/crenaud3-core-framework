package dialogue;

import javafx.scene.image.Image;
import javafx.scene.media.Media;

import java.io.IOException;
import java.io.InputStream;

/**
 * Encapsulate the DialogueBuilder for easier use
 * Package private
 * @author klmp200
 */
class DialogueManager {
    private DialogueBuilder builder;
    private Integer dialogueNb = 0;

    /**
     * Create a new DialogueManager fro ma given config file
     * @param configFile as InputStream
     */
    public DialogueManager(InputStream configFile) throws IOException {
        this.builder = new DialogueBuilder(configFile);
    }


    /**
     * Check if another dialogue is available
     * @return boolean
     */
    public boolean hasNextDialogue() {
        return dialogueNb < this.numberDialogues();
    }

    /**
     * Return number of dialogues
     * @return integer
     */
    private int numberDialogues() {
        return (builder.getDialogueSteps() != null) ? builder.getDialogueSteps().size() : 0;
    }

    /**
     * Skip a dialogue
     */
    public void skipDialogue() {
        this.dialogueNb++;
    }

    /**
     * Get next available dialogue
     * @return DialogueStep
     */
    public DialogueStep getNextDialogue() {
        DialogueStep d = this.builder.getDialogueSteps().get(this.dialogueNb);
        this.dialogueNb++;
        return d;
    }

    /**
     * Get a music from the music list of the dialogue
     * @param id as integer
     * @return Media
     */
    public Media getMusic(int id) {
        if (id >= 0 && id < builder.getMusics().size() && builder.getMusics().size() > 0)
            return this.builder.getMusics().get(id);
        return null;
    }

    /**
     * Get a character from the character list of the dialogue
     * @param id as integer
     * @return DialogueCharacter
     */
    public DialogueCharacter getCharacter(int id){
        if (id >= 0 && id < this.builder.getCharacters().size())
            return this.builder.getCharacters().get(id);
        return null;
    }

    /**
     * Get a background from the background list of the dialogue
     * @param id as integer
     * @return Image
     */
    public Image getBackground(int id){
        if (id >= 0 && id < this.builder.getBackgrounds().size())
            return this.builder.getBackgrounds().get(id);
        return null;
    }

}
