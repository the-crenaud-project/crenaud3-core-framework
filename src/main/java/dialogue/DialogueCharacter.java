package dialogue;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import javafx.scene.image.Image;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Character for a dialogue
 * Package private
 * @author klmp200
 */
class DialogueCharacter {

    private InputStream fileStream;
    private String firstName;
    private String lastName;
    private Map<String, DialogueSprite> sprites = new HashMap<>();

    /**
     * Creates a new character for dialogue
     * @param path of the json file
     * @throws IOException if json malformed
     */
    public DialogueCharacter(String path) throws IOException {
        fileStream = getClass().getResourceAsStream(path);
        this.parse();
    }

    /**
     * Function that read the json and generate the character
     * @throws IOException if json is malformed
     */
    private void parse() throws IOException {
        Gson g = new Gson();
        JsonReader reader = g.newJsonReader(new InputStreamReader(fileStream));
        String name = null;

        reader.beginObject();
        while (reader.hasNext()){
            name = reader.nextName();
            switch (name) {
                case "first name":
                    this.firstName = reader.nextString();
                    break;
                case "last name":
                    this.lastName = reader.nextString();
                    break;
                case "sprites":
                    this.parseSprites(reader);
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
    }

    /**
     * Generate a sprite map according to a json
     * @param reader a JsonReader
     * @throws IOException if json is malformed
     */
    private void parseSprites(JsonReader reader) throws IOException {
        String behavior = null;
        DialogueSprite s = null;

        reader.beginObject();
        while (reader.hasNext()){
            behavior = reader.nextName();
            s = new DialogueSprite(reader.nextString());
            this.sprites.put(behavior, s);
        }
    }

    /**
     * Get first name of the character
     * @return first name as String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get last name of the character
     * @return last name as String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Get sprite according to a given behavior
     * Return null if behavior not found
     * @param behavior as String
     * @return Imago from DialogueSprite
     */
    public Image getSprite(String behavior){
        DialogueSprite s = sprites.get(behavior);
        if (s != null)
            return s.getImg();
        return null;
    }

    /**
     * Convert to a String
     * @return String
     */
    @Override
    public String toString() {
        return "DialogueCharacter{" +
                "fileStream=" + fileStream +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", sprites=" + sprites.keySet().toString() +
                '}';
    }
}
