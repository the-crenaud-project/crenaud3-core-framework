package cameratest;

import graphicalengine.Camera;
import graphicalengine.Sprite;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import scenecontroller.Scene;


public class TestController extends Scene {
    @FXML private Canvas cv;
    private Camera cam = null;
//    private Sprite sp;
  //  private Sprite located;

    public TestController(){
        fxmlPath = "cameratest/test.fxml";
        }

    @Override
    public void end() {

    }

    public void draw(){
        cv.getGraphicsContext2D().clearRect(0,0,cv.getWidth()*1/cam.getZoom(),cv.getHeight()*1/cam.getZoom());
    //    cam.drawSomething(sp);
      //  cam.drawSomething(located);

    }

    public void mouseClick(MouseEvent e){
        if (cam == null){
            cam = new Camera(cv);
        //    sp = new Sprite("/cameratest/palette.png",new Point2D(50,50));
          //  located = new Sprite("/cameratest/palette.png",new Point2D(50,50));
        }
        //located.setPosition(cam.fromCanvasToCameraView(new Point2D(e.getX(),e.getY())));

        draw();

    }

    public void flipX(){
        //sp.setFlip(!sp.isFlippedX(),sp.isFlippedY());
        draw();
    }
    public void flipY(){
        //sp.setFlip(sp.isFlippedX(),!sp.isFlippedY());
        draw();
    }

    public void zooming(ScrollEvent e){
        cam.multiplyZoom(1+((float) e.getDeltaY()/100));
        System.out.println(cam.getZoom());

        draw();
    }

    public void zoom1(){
        cam.setZoom(1);
        System.out.println(cam.getZoom());
        draw();
    }

    public void onKeyboardInput(KeyEvent e){
        if(e.getCode().isArrowKey()){
            switch (e.getCode()){
                case UP:
                    cam.move(new Point2D(0,1));
                    break;
                case DOWN:
                    cam.move(new Point2D(0,-1));
                    break;
                case LEFT:
                    cam.move(new Point2D(1,0));
                    break;
                case RIGHT:
                    cam.move(new Point2D(-1,0));

            }
        }
       draw();
    }

}
